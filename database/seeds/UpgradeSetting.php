<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

//TODO: @execute [php artisan db:seed --class=UpgradeSetting]
class UpgradeSetting extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => 'scheduledOrderStatusText',
            'value' => 'Sheduled Orden'
        ]);
    }
}