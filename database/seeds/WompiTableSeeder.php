<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Artisan;

//TODO: @execute [php artisan db:seed --class=WompiTableSeeder]
class WompiTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pg = DB::table('payment_gateways')->insert([
            'name' => 'Wompi',
            'description' => 'Wompi Payment Gateway',
            'is_active' => 0,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        if($pg) {
            $s = DB::table('settings')->insert([
                [
                    'key' => 'wompiPublicKey',
                    'value' => 'pub_test_5hS5Oa7MqC0kFnSYdjodjwIFSbsdxNNM'
                ],
                [
                    'key' => 'wompiPrivateKey',
                    'value' => 'prv_test_aF23qPITsf5KEgWp9EJSJIqrwus2eC58'
                ]
            ]);
        }
    }
}
