import React, { useEffect } from "react";

const Wompi = (props) => {
    //const [state, setState] = useState(0);
    const {wompiPublicKey, COP, amountInCents, reference, onProcess, onProcessError, json} = props;
    window.localStorage.setItem('wompi_url', JSON.stringify(json));

    const data = {
      currency: COP,
      amountInCents: amountInCents,
      reference: reference,
      publicKey: wompiPublicKey,
      redirectUrl: window.location.protocol+ '//'+ window.location.host + '/checkoutv2'
    };

    useEffect(() => {
      const script = document.createElement("script");
      script.src = "https://checkout.wompi.co/widget.js";
      script.type="text/javascript"
      document.body.appendChild(script);   
      
    }, [])

    const render = () => {
      execute()
    }

    const execute =  () => {
      let ct = new window.WidgetCheckout(data);
      ct.open((result) => {
        let transaction = result.transaction
        if(transaction.status == "APPROVED"){
          onProcess()
        } else {
          onProcessError()
        }
        /*console.log('Transaction ID: ', transaction.id)
        console.log('Transaction ID: ', transaction.status)
        console.log('Transaction object: ', transaction)*/
      })
    }
    
    return (
      <>
        <div onClick={()=>render()} className="block block-link-shadow text-left shadow-light">
          <div  className="block-content block-content-full clearfix py-3 payment-select-block">
            <div className="float-right mt-10">
              <img src="https://wompi.co/wp-content/themes/wp-theme-wompi/dist/images/logo-bancolombia_76ea6dfa.svg" alt="Wompi" className="img-fluid"></img>
            </div>
            <div className="font-size-h3 font-w600">
              Wompi
            </div>
            <div className="font-size-sm font-w600 text-muted">
              Pay with cards
            </div>
          </div>
        </div>
      </>
    );
  }

  export default Wompi;