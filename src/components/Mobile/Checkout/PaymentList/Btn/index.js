import React, {Component} from "react";
import Axios from "axios";
import { PLACE_ORDER_URL } from "../../../../../configs";
import { updateCart } from "../../../../../services/total/actions";

class BtnComponent extends Component {
  static contextTypes = {
		router: () => null,
	};

  state = {
    stripe_opened: false,
		placeOrderError: false,
		errorMessage: "",
  };
  
  componentDidMount() {
    const URL = this.props.history.location.search;
    const STATE_WOMPI = this.getParameterByName('id', URL);
    this.getStatusWoompi(STATE_WOMPI)
  }

  getStatusWoompi (id){
    return Axios.get('https://sandbox.wompi.co/v1/transactions/'+id)
      .then((response) => {
        let r = response.data;
        if(r.data.status == "APPROVED"){
          this.__placeOrder("", "WOMPI")
        }
      }).catch(function(error) {
        console.log(error);
      });
  }

  __placeOrder = (payment_token, method) => {
    const { user, cartProducts, coupon, cartTotal } = JSON.parse(window.localStorage.getItem("wompi_url"));
    console.log(JSON.parse(window.localStorage.getItem("wompi_url")))
		if (user.success) {
			if (localStorage.getItem("userSelected") === "SELFPICKUP") {
					this.placeOrder(
						user,
						cartProducts,
						coupon,
						JSON.parse(localStorage.getItem("userSetAddress")),
						localStorage.getItem("orderComment"),
						cartTotal,
						method,
						payment_token,
						2,
						this.state.walletChecked,
						this.state.distance,
						localStorage.getItem("getDateTime")
					);
			} else {
				this.placeOrder(
						user,
						cartProducts,
						coupon,
						JSON.parse(localStorage.getItem("userSetAddress")),
						localStorage.getItem("orderComment"),
						cartTotal,
						method,
						payment_token,
						1,
						this.state.walletChecked,
						this.state.distance,
						localStorage.getItem("getDateTime")
					);
			}
		}
  };
  
  placeOrder (user, order, coupon,
    location, order_comment, total, method,
    payment_token, delivery_type, partial_wallet,
    distance, datetime) {
    return Axios.post(PLACE_ORDER_URL, {
      token: user.data.auth_token,
      user: user,
      order: order,
      coupon: coupon,
      location: location,
      order_comment: order_comment,
      total: total,
      method: method,
      payment_token: payment_token,
      delivery_type: delivery_type,
      partial_wallet: partial_wallet,
      dis: distance,
      programe: datetime
    })
      .then((response) => {
        const checkout = response.data;
        if (checkout.success) {
          //dispatch({ type: "PLACE_ORDER", payload: checkout });
          //const state = getState();
          // console.log(state);
          const cartProducts = [] //state.cart.products;
          localStorage.removeItem("orderComment");
          localStorage.removeItem("getDateTime");
          localStorage.removeItem("wompi_url");
  
          for (let i = cartProducts.length - 1; i >= 0; i--) {
            // remove all items from cart
            cartProducts.splice(i, 1);
          }
  
          //dispatch(updateCart(cartProducts));
          localStorage.removeItem("appliedCoupon");
          const coupon = [];
          //dispatch({ type: "APPLY_COUPON", payload: coupon });
          this.setState({ stripe_opened: false });

          let state = JSON.parse(window.localStorage.getItem('state'));
          state.cart.products = [];
          state.total.data.productQuantity = 0;
          state.total.data.totalPrice = 0;
          window.localStorage.setItem('state', JSON.stringify(state));
          return window.location.href = window.location.protocol+ '//'+ window.location.host +'/running-order/'+checkout.data.unique_order_id;
        } else {
          return checkout;
        }
      })
      .catch(function(error) {
        this.setState({ placeOrderError: true, errorMessage: error });
      });
  }


  getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
  }

  render() {
    return (
      <React.Fragment>
        <div className="text-center">
          <h1>
            Procesando ...............
          </h1>
          </div>
      </React.Fragment>
    );
  }
} 

export default BtnComponent