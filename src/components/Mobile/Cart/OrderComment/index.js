import React, { Component } from "react";

class OrderComment extends Component {
    state = {
        comment: "",
        datetime: ""
    };

    componentDidMount() {
        this.setState({ 
            comment: localStorage.getItem("orderComment"),
            datetime: localStorage.getItem("getDateTime")
        })
    }
    
    handleInput = event => {
        this.setState({ comment: event.target.value });
        localStorage.setItem("orderComment", event.target.value);
    };

    handleDateTime = event =>{
        this.setState({datetime : event.target.value })
        localStorage.setItem("getDateTime", event.target.value);
    }

    render() {
        return (
            <React.Fragment>
                <input
                    className="form-control order-comment mb-20"
                    type="text"
                    placeholder={localStorage.getItem("cartSuggestionPlaceholder")}
                    onChange={this.handleInput}
                    value={this.state.comment || ""}
                />

                <div className="block-content block-content-full bg-white pt-10 pb-5">
                    <h2 className="item-text mb-10">¿Programar Orden?</h2>
                    <input 
                        className="order-comment form-control" 
                        value={this.state.datetime} 
                        type="datetime-local"
                        onChange={this.handleDateTime} />
                </div>
            </React.Fragment>
        );
    }
}

export default OrderComment;
